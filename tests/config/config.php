<?php
/**
 * Project jpush-server
 * file: config.php
 * User: weblinuxgame
 * Date: 2019/6/27
 * Time: 17:30
 */

return [
    // 应用集合
    'apps' => [
        // 默认应用
        'default' => [
            'app_key' => env('app_key'),
            'master_secret' => env('master_secret'),
            'log_file' => storage_path(env('log_file', 'storage/logs/jpush.log')),
        ],
        // 可以应用自定义
        'im' => [
            'app_key' => '应用key',
            'master_secret' => '应用秘钥',
            'zone' => null,
            'retry_times' => 3,
            'log_file' => 'storage/logs/jpush.log',
        ]
    ],
    // 以下配置可以全局|可以局部
    'zone' => null,
    'retry_times' => 3,
    'log_file' => storage_path('/storage/logs/jpush.log'),
    'register' => \WebLinuxGame\JPush\Contracts\JPushRegister::class, // 请设置关联逻辑类
];