极光推送
==================

![license](https://img.shields.io/badge/php-^7.13-red.svg) 
![license](https://img.shields.io/badge/push-server-green.svg)
![license](https://img.shields.io/badge/license-MIT-yellow.svg)
![license](https://img.shields.io/badge/composer-package-green.svg)

基于 极光推送官方 包封装 (^php7.13)

 [极光官方文档](https://docs.jiguang.cn//jpush/server/sdk/php_sdk/)

>  1.安装       
    
     composer require weblinuxgame\jpush-server
    
>  2.相关配置 定义

  config.php
        
       return [
            // 应用集合
            'apps' => [
                // 默认应用
                'default' => [
                    'app_key' => '应用key',
                    'master_secret' => '应用秘钥',
                ],
                // 可以应用自定义
                'im' => [
                    'app_key' => '应用key',
                    'master_secret' => '应用秘钥',
                    'zone' => null,
                    'retry_times' => 3,
                    'log_file' => 'storage/logs/jpush.log',
                ]
            ],
            // 以下配置可以全局|可以局部
            'zone' => null,
            'retry_times' => 3,
            'log_file' => 'storage/logs/jpush.log',
            'register' => \WebLinuxGame\JPush\Contract\JPushRegister::class, // 请设置关联逻辑类
        ];


>  3.示例  

    $config = [...];
    $oService = new JPushService($config);
    $client = $oService->client();
    
    