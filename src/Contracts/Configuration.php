<?php
/**
 * Project jpush-server
 * file: Configuration.php
 * User: weblinuxgame
 * Date: 2019/6/27
 * Time: 16:55
 */

namespace WebLinuxGame\JPush\Contracts;

/**
 * 配置
 * Interface Configuration
 * @package WebLinuxGame\JPush\Contracts
 */
interface Configuration extends \ArrayAccess, \Serializable
{
    public function __isset($name);

    public function __get($name);

    public function __set($name, $value);

    public function toArray(): array;

    public function get(string $key, $default = null);

    public function set(string $key, $value);

}