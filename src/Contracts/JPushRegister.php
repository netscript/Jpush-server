<?php
/**
 * Project jpush-server
 * file: JPushRegister.php
 * User: weblinuxgame
 * Date: 2019/6/27
 * Time: 18:31
 */

namespace WebLinuxGame\JPush\Contracts;

/**
 * 注册用户关联
 * Interface JPushRegister
 * @package WebLinuxGame\JPush\Contracts
 */
interface JPushRegister
{
    public function handle(array $data = []);
}