<?php
/**
 * Project jpush-server
 * file: MessageEnum.php
 * User: weblinuxgame
 * Date: 2019/6/28
 * Time: 11:12
 */

namespace WebLinuxGame\JPush\Enums;

/**
 * 消息类型
 * Interface MessageEnum
 * @package WebLinuxGame\JPush\Enums
 */
interface MessageEnum
{
    const TYPE_TEXT = 'text';
    const TYPE_URL = 'url';
    const TYPE_XML = 'xml';
    const TYPE_JSON = 'json';
    const TYPE_PAGE = 'page';
    const TYPE_ALERT = 'alert';
    const TYPE_BASE64 = 'base64';
    const TYPE_ACTION = 'action';
    const TYPE_IMAGE_URL = 'image';
    const TYPE_ENCRYPT = 'encrypt';
    const TYPE_ENCRYPT_SHA1 = 'sha1';
    const TYPE_URL_ENCODE = 'urlencode';
    const TYPE_IMAGE_BASE64 = 'image-base64';
}