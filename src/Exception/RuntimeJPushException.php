<?php
/**
 * Project jpush-server
 * file: RuntimeJPushException.php
 * User: weblinuxgame
 * Date: 2019/6/27
 * Time: 17:20
 */

namespace WebLinuxGame\JPush\Exception;

/**
 * 运行异常
 * Class RuntimeJPushException
 * @package WebLinuxGame\JPush\Exception
 */
class RuntimeJPushException extends \Exception
{

}