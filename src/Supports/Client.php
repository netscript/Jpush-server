<?php
/**
 * Project jpush-server
 * file: Client.php
 * User: weblinuxgame
 * Date: 2019/6/27
 * Time: 17:47
 */

namespace WebLinuxGame\JPush\Supports;

use JPush\Config;
use JPush\Client as BaseClient;

/**
 * 客户端
 * Class Client
 * @package WebLinuxGame\JPush\Supports
 */
class Client extends BaseClient
{
    /**
     * @var \WebLinuxGame\JPush\Supports\Configure
     */
    protected $config;

    /**
     * 构造
     * Client constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->config = new Configure($config);

        parent::__construct(
            $this->config->app_key,
            $this->config->master_secret,
            $this->config->get('log_file', Config::DEFAULT_LOG_FILE),
            $this->config->get('retry_times', Config::DEFAULT_MAX_RETRY_TIMES),
            $this->config->get('zone')
        );
    }

    /**
     * 关闭日志
     * @return $this
     */
    public function logOff()
    {
        $this->config['log_off'] = true;
        return $this;
    }

    /**
     * 开启日志
     * @return $this
     */
    public function logOn()
    {
        $this->config['log_off'] = false;
        return $this;
    }

    /**
     * 获取日志文件
     * @return null|string
     */
    public function getLogFile()
    {
        if(!empty($this->config->log_off)){
            return null;
        }
        return parent::getLogFile();
    }

}