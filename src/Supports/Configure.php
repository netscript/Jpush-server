<?php
/**
 * Project jpush-server
 * file: Configure.php
 * User: weblinuxgame
 * Date: 2019/6/27
 * Time: 16:59
 */

namespace WebLinuxGame\JPush\Supports;

use ArrayObject;
use WebLinuxGame\JPush\Contracts\Configuration;

/**
 * 配置对象
 * Class Configure
 * @package WebLinuxGame\JPush\Supports
 */
class Configure extends ArrayObject implements Configuration
{
    /**
     * 初始化
     * Configure constructor.
     * @param array $input
     */
    public function __construct($input = [])
    {
        if (!is_object($input) && $input instanceof Configuration) {
            $input = $input->toArray();
        }
        if (!is_array($input) || empty($input)) {
            $input = [];
        }
        parent::__construct($input);
    }

    /**
     * 配置键值对是否存在
     * @param $name
     * @return bool
     */
    public function __isset($name)
    {
        return $this->offsetExists($name);
    }

    /**
     * 配置值获取
     * @param $name
     * @return bool
     */
    public function __get($name)
    {
        return $this->offsetGet($name);
    }

    /**
     * 配置值设置
     * @param $name
     * @param $value
     * @return void
     */
    public function __set($name, $value)
    {
        $this->offsetSet($name, $value);
    }

    /**
     * 转换数组
     * @return array
     */
    public function toArray(): array
    {
       return $this->getArrayCopy();
    }

    /**
     * 获取
     * @param string $key
     * @param null $default
     * @return mixed
     */
    public function get(string $key, $default = null)
    {
        return Arr::get($this,$key,$default);
    }

    /**
     * 设置
     * @param string $key
     * @param $value
     * @return $this
     */
    public function set(string $key, $value)
    {
        Arr::set($this,$key,$value);
        return $this;
    }
}